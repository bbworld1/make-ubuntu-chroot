#!/bin/sh
# Installs dependencies for ros-chroot-creator.

# Contribution idea: Add more!
echo "Installing schroot+debootstrap..."
if [ -f /usr/bin/pacman ]; then
    sudo pacman -S schroot debootstrap
elif [ -f /usr/bin/yum ]; then
    sudo yum install schroot debootstrap
elif [ -f /usr/bin/emerge ]; then
    sudo emerge --ask dev-util/schroot dev-util/debootstrap
else
    echo "Sorry, looks like your package manager isn't supported by the script yet."
    echo "Don't worry - you can still install dependencies manually (find out how to install schroot and debootstrap on your system)."
    echo "If you'd like to contribute build instructions for your package manager, we'd love to have your PR at https://gitlab.com/bbworld1/ros-chroot-creator!"
fi

sudo install -m 755 make-ubuntu-chroot /usr/local/bin
