# make-ubuntu-chroot: make an ubuntu chroot

make-ubuntu-chroot does exactly what it sounds like: it automatically makes an Ubuntu chroot.
Simply install it with `install.sh` and use it like so:

`make-ubuntu-chroot bionic ubuntu-bionic`

This example makes an Ubuntu Bionic (18.04) chroot named ubuntu-bionic, but you can name your chroot anything (follow terminal-sane file names, of course) and use any Ubuntu distro.

## Install

./install.sh

## Usage

See above.

## Contributing

PRs welcome! Fork it, raise an issue, PR it.

## Issues/Bug Reporting

Issue tracker is on the left.
